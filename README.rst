==============
stringdbscores
==============


.. image:: https://img.shields.io/pypi/v/stringdbscores.svg
        :target: https://pypi.python.org/pypi/stringdbscores
..
 image:: https://img.shields.io/travis/MorfeoRenai/stringdbscores.svg
        :target: https://travis-ci.com/MorfeoRenai/stringdbscores
..
.. image:: https://readthedocs.org/projects/stringdbscores/badge/?version=latest
        :target: https://stringdbscores.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




``stringdbscores`` is a Python package that queries a Protein-Protein Interaction (PPI) network
from the `STRING database`_, starting from a given gene set (so called "seed"). It can compute
scores and ranks for every gene node in the network using graph-based metrics, given the starting
"seed" gene set.

It was developed more as an exercise and demo. It could still be useful in studies of PPI
networks though.

* Free software: GNU General Public License v3
* Documentation: https://stringdbscores.readthedocs.io.

.. _STRING database: https://string-db.org/

Features
--------

* TODO

Install
-------

Basic Usage
-----------

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
