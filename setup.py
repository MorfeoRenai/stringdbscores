#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [ ]

test_requirements = [ ]

setup(
    author="Alessandro Pandolfi",
    author_email='alessandro.pandolfi@protonmail.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="`stringdbscores` is a tool that queries a Protein-Protein Interaction (PPI) network from the STRING database, starting from a seed gene set. It also can rank another set of genes, so called \"candidate genes\", in the network according to a graph-based metric.",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='stringdbscores',
    name='stringdbscores',
    packages=find_packages(include=['stringdbscores', 'stringdbscores.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/MorfeoRenai/stringdbscores',
    version='0.1.0',
    zip_safe=False,
)
